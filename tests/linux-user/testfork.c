#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

static void test_fork(void)
{
    pid_t id;

    id = fork();
    if (id == 0) {
        printf("child\n");
        exit(0);
    } else if (id == -1) {
        perror("Fork fail");
        exit(-1);
    } else {
        int status;
        waitpid(id, &status, 0);
        printf("Parent:  done %d\n", status);
        exit(0);
    }
}

int main(int argc, char **argv)
{
    test_fork();
    return 0;
}
